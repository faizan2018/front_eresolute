import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoggedInGuard } from './guards/logged-in.guard';
import { HomeComponent } from './components/client/home/home.component';
import { RegisterComponent } from './components/client/register/register.component';
import { DashboardComponent } from './components/client/dashboard/dashboard.component';
import { UserListComponent } from './components/client/user-list/user-list.component';
import { UpdateUserComponent } from './components/client/update-user/update-user.component';


const routes: Routes = [
  {
    path: '',
      component: HomeComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'userList/:id',
    component: UserListComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'updateUser/:id',
    component: UpdateUserComponent,
    canActivate: [LoggedInGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
