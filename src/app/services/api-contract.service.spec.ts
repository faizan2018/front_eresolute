import { TestBed, inject } from '@angular/core/testing';

import { ApiContractService } from './api-contract.service';

describe('ApiContractService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiContractService]
    });
  });

  it('should be created', inject([ApiContractService], (service: ApiContractService) => {
    expect(service).toBeTruthy();
  }));
});
