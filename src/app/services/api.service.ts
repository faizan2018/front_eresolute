import { Injectable } from '@angular/core';
import { ApiContractService } from './api-contract.service';

@Injectable()
export class ApiService {

  constructor(private api: ApiContractService) { }

  contactForm(obj): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('api/contact', obj)
        .then(resolve)
        .catch(reject)
    })
  }

  subscribe(email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('api/subscribe', { email })
        .then(resolve)
        .catch(reject)
    })
  }

  getUserStatus(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('dashboard', true)
        .then(resolve)
        .catch(reject)
    })
  }

  getUserList(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('userList?page='+id, true)
        .then(resolve)
        .catch(reject)
    })
  }

  approveUser(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('approveUser/'+id, true)
        .then(resolve)
        .catch(reject)
    })
  }

  disapproveUser(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('disapproveUser/'+id, true)
        .then(resolve)
        .catch(reject)
    })
  }

   deleteUser(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('deleteUser/'+id, true)
        .then(resolve)
        .catch(reject)
    })
  }

  switchRole(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('switchRole/'+id, true)
        .then(resolve)
        .catch(reject)
    })
  }

  updateUser(id, data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('updateUser/'+id, data, true)
        .then(resolve)
        .catch(reject)
    })
  }

  getProductDetails(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('api/product/' + id)
        .then(resolve)
        .catch(reject)
    })
  }

  authorizePayment(data): Promise<any> {
    return new Promise((resolve, reject) =>
      this.api.post('api/verifypayment', data, true)
        .then(resolve)
        .catch(reject)
    )
  }

  postAddress(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('api/newaddress', data, true)
      .then(resolve)
      .catch(reject)
    })
  }

  getRecommededProducts(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('api/recommendedproducts')
      .then(resolve)
      .catch(reject)
    })
  }

  getOrders(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('api/listorders', {}, true)
      .then(resolve)
      .catch(reject)
    })
  }

  getBlogPosts(): Promise<any> {
    return new Promise((resolve,reject) => {
      this.api.get('api/blog/posts')
      .then(resolve)
      .catch(reject)
    })
  }

  getBlogPost(id): Promise<any> {
    return new Promise((resolve,reject) => {
      this.api.get('api/blog/post/' + id)
      .then(resolve)
      .catch(reject)
    })
  }

  getAlsoLike(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('api/alsolike/' + id)
      .then(resolve)
      .catch(reject)
    })
  }

}
