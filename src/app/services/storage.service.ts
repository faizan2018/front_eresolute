import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }

  get(key) {
      return localStorage.getItem(key);
  }

  set(key, value) {
      localStorage.setItem(key, value);
  }

  remove(key) {
      localStorage.removeItem(key);
  }

  clear() {
      localStorage.clear();
  }

}
