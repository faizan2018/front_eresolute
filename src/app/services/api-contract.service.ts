import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { environment } from '../../environments/environment';
const axios = require('axios');

@Injectable()
export class ApiContractService {

  constructor(
      private storage: StorageService
  ) { }

  buildUrl(location) {
    const protocol = environment.api.ssl ? 'https' : 'http';
    const host = environment.api.host;
    const port = environment.api.port;
    const url = `${protocol}://${host}:${port}/${location}`;
    return url;
  }

 

  public get(location, options: boolean | {}  = false): Promise<any> {
    let url = this.buildUrl(location);
    options = options ? this.addHeaders() : {};
    return new Promise((resolve, reject) => {
      axios.get(url, options)
        .then(function(response) {
          resolve(response);
        })
        .catch(function(error) {
          reject(error);
        })
    })
  }

  public delete(location): Promise<any> {
    let url = this.buildUrl(location);
    return new Promise((resolve, reject) => {
      axios.delete(url)
        .then(function(response) {
          resolve(response);
        })
        .catch(function(error) {
          reject(error);
        })
    })
  }

  public post(location, data, options: boolean | {}  = false): Promise<any> {
    let url = this.buildUrl(location);
    options = options ? this.addHeaders() : {};
    return new Promise((resolve, reject) => {
      axios.post(url, data, options)
        .then(function(response) {
          resolve(response);
        })
        .catch(function(error) {
          reject(error);
        })
    })
  }

  public put(location, data): Promise<any> {
    let url = this.buildUrl(location);
    return new Promise((resolve, reject) => {
      axios.put(url, data)
        .then(function(response) {
          resolve(response);
        })
        .catch(function(error) {
          reject(error);
        })
    })
  }

  addHeaders(): {} {
    let token = this.storage.get('api_token');
    return {
      headers: {
        Authorization: 'bearer' + ' ' + token
       // Accept: 'application/json'
      }
    };
  }

}
