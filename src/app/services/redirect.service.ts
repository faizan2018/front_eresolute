import { Injectable } from '@angular/core';

@Injectable()
export class RedirectService {

  public isRedirectToCheckout: boolean = false;
  constructor() { }

  redirectToCheckout(value) {
    this.isRedirectToCheckout = value;
  }

}
