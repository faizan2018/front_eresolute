import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AuthenticationService } from '../../../services/authentication.service';
import { ApiContractService } from '../../../services/api-contract.service';
import { RedirectService } from '../../../services/redirect.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	registerForm: FormGroup;
  loginForm: FormGroup;
  isInvalid: boolean = false;
  message: boolean = false;

  constructor(
  	private auth: AuthenticationService,
    private fb: FormBuilder,
    private spinnerService: Ng4LoadingSpinnerService,
    private router: Router,
    private api: ApiContractService,
    private redirect: RedirectService
  	) { 

  	this.registerForm = new FormGroup({
      name: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      }),
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
        updateOn: 'blur'
      }),
      password: new FormControl('', {
        validators: [Validators.required, Validators.minLength(6), Validators.maxLength(15)],
        updateOn: 'blur'
      }),
      cnfrmPassword: new FormControl('')
    })

  }

  public register() {
    if(this.registerForm.invalid) return;
    let values = this.registerForm.value;
    if (values.password === values.cnfrmPassword) {
      this.spinnerService.show();
      this.auth.register(values)
        .then(res => {
          this.spinnerService.hide();
          this.message = res.data.output;
          this.router.navigate(['/']);
        })
        .catch(err => {
          this.spinnerService.hide();
        });
    }
    else {
      this.isInvalid = true;
      setTimeout(() => {
        this.isInvalid = false;
      }, 5000)
    }
  }


  ngOnInit() {
  }

}
