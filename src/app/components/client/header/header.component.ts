import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	loggedIn: boolean
  constructor(
  	private auth: AuthenticationService
  	) {

  	 }

  logout() {
    this.auth.logout();
  }


  ngOnInit() {

  	this.auth.loggedIn().subscribe(res => {
      this.loggedIn = res;
    });
  }

}
