import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
	public userDetails;
  constructor(private api: ApiService) {
  	this.userDetails = {
   	name: '',
   	role: '',
   	approval: ''
   }
   }




  ngOnInit() {
  	this.api.getUserStatus()
      .then(res => {
        this.userDetails = res.data;
      })
      .catch()
  }

}
