import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AuthenticationService } from '../../../services/authentication.service';
import { ApiContractService } from '../../../services/api-contract.service';
import { RedirectService } from '../../../services/redirect.service';
import { ActivatedRoute } from "@angular/router";
import { ApiService } from '../../../services/api.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  registerForm: FormGroup;
  loginForm: FormGroup;
  isInvalid: boolean = false;
  message: boolean = false;
  private activeRoute;
  constructor(
  	private auth: AuthenticationService,
    private fb: FormBuilder,
    private spinnerService: Ng4LoadingSpinnerService,
    private router: Router,
    private apiCont: ApiContractService,
    private redirect: RedirectService,
    private route: ActivatedRoute,
    private api: ApiService
  	) { 

  	this.route.params.subscribe(params => {
      this.activeRoute = params;
    })

  	this.registerForm = new FormGroup({
      name: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      }),
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
        updateOn: 'blur'
      }),
      password: new FormControl('', {
        validators: [Validators.required, Validators.minLength(6), Validators.maxLength(15)],
        updateOn: 'blur'
      }),
      cnfrmPassword: new FormControl('')
    })
  }

  public update() {
    if(this.registerForm.invalid) return;
    let values = this.registerForm.value;
    if (values.password === values.cnfrmPassword) {
      this.spinnerService.show();
      this.api.updateUser(this.activeRoute.id,values)
        .then(res => {
          this.spinnerService.hide();
          this.message = res.data.output;
          this.router.navigate(['userList/1']);
        })
        .catch(err => {
          this.spinnerService.hide();
        });
    }
    else {
      this.isInvalid = true;
      setTimeout(() => {
        this.isInvalid = false;
      }, 5000)
    }
  }

  ngOnInit() {
  }

}
