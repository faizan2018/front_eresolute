import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AuthenticationService } from '../../../services/authentication.service';
import { ApiContractService } from '../../../services/api-contract.service';
import { RedirectService } from '../../../services/redirect.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	loginForm: FormGroup;
	loggedIn : boolean =false;
  constructor(
  	private auth: AuthenticationService,
    private fb: FormBuilder,
    private spinnerService: Ng4LoadingSpinnerService,
    private router: Router,
    private api: ApiContractService,
    private redirect: RedirectService
    ) { 

  	this.loginForm = new FormGroup({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
        updateOn: 'blur'
      }),
      password: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      })
    })
  }

changeLinks(){
  	if(this.auth.isActiveUser()) {
  		this.loggedIn = true;
  	}
  }

  public login(loginCreds = null) {
    if(this.loginForm.invalid && loginCreds == null) return;
    this.spinnerService.show();
    let values = this.loginForm.value;
    values = loginCreds ? loginCreds : values;
    this.auth.login(values)
      .then(res => {
        this.spinnerService.hide();
        	this.changeLinks();
          this.router.navigate(['/dashboard']);
       		
      })
      .catch(err => {
        this.spinnerService.hide();
      })
  }


  ngOnInit() {
  }

}
