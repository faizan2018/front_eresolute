import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { ActivatedRoute } from "@angular/router";
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
	public userList;
	private activeRoute;
	public userDetails;
  constructor(private api: ApiService,
  	private route: ActivatedRoute,
  	private router: Router) { 
  	this.route.params.subscribe(params => {
      this.activeRoute = params;
    })
    this.userDetails = {
   	name: '',
   	role: '',
   	approval: ''
   }

   this.userList = {
   		message: ''
   }
  }

  nextList(){
  	this.api.getUserList(this.activeRoute.id++)
      .then(res => {
        this.userList = res.data;
      })
      .catch()
      return false;
  }

  prevList(){
  	this.api.getUserList(this.activeRoute.id--)
      .then(res => {
        this.userList = res.data;
      })
      .catch()
      return false;
  }

  approveUser(id){
  	this.api.approveUser(id)
      .then(res => {
        this.router.navigate(['userList/'+this.activeRoute.id]);
        this.api.getUserList(this.activeRoute.id)
      .then(res => {
        this.userList = res.data;
      })
      .catch()
      })
      .catch()
  }
  disapproveUser(id){
  	this.api.disapproveUser(id)
      .then(res => {
        this.router.navigate(['userList/'+this.activeRoute.id]);
        this.api.getUserList(this.activeRoute.id)
      .then(res => {
        this.userList = res.data;
      })
      .catch()
      })
      .catch()
  }

  switchRole(id){
  	this.api.switchRole(id)
      .then(res => {
        this.router.navigate(['userList/'+this.activeRoute.id]);
        this.api.getUserList(this.activeRoute.id)
      .then(res => {
        this.userList = res.data;
      })
      .catch()
      })
      .catch()
  }

  deleteUser(id){
  	this.api.deleteUser(id)
      .then(res => {
        this.router.navigate(['userList/'+this.activeRoute.id]);
        this.api.getUserList(this.activeRoute.id)
      .then(res => {
        this.userList = res.data;
      })
      .catch()
      })
      .catch()
  }

  ngOnInit() {
  	

  	this.api.getUserList(this.activeRoute.id)
      .then(res => {
        this.userList = res.data;
      })
      .catch()

      this.api.getUserStatus()
      .then(res => {
        this.userDetails = res.data;
      })
      .catch()
  }

}
