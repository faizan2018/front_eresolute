import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './components/client/layout/layout.component';
import { HeaderComponent } from './components/client/header/header.component';
import { FooterComponent } from './components/client/footer/footer.component';
import { ApiService } from './services/api.service';
import { AuthenticationService } from './services/authentication.service';
import { ApiContractService } from './services/api-contract.service';
import { StorageService } from './services/storage.service';
import { RedirectService} from './services/redirect.service';
import { LoggedInGuard } from './guards/logged-in.guard';
import { HomeComponent } from './components/client/home/home.component';
import { RegisterComponent } from './components/client/register/register.component';
import { DashboardComponent } from './components/client/dashboard/dashboard.component';
import { UserListComponent } from './components/client/user-list/user-list.component';
import { UpdateUserComponent } from './components/client/update-user/update-user.component';



@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    RegisterComponent,
    DashboardComponent,
    UserListComponent,
    UpdateUserComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng4LoadingSpinnerModule.forRoot()

  ],
  providers: [
    ApiService,
    ApiContractService,
    AuthenticationService,
    StorageService,
    LoggedInGuard,
    RedirectService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
