import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../services/authentication.service';
import { RedirectService } from '../services/redirect.service';
import * as toastr from 'toastr';

@Injectable()
export class LoggedInGuard implements CanActivate {

  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private redirect: RedirectService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(this.auth.isActiveUser()) {
      return true;
    }

    else {
      toastr.info('Please login to continue')
      // if(state.url == "/checkout") {
      //   this.redirect.redirectToCheckout(true);
      // }
      this.router.navigateByUrl('/');
      return false
    };
  }
}
